package RSA

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"github.com/enzoic/enzoic-go-client"
	passwordvalidator "github.com/wagslane/go-password-validator"
	"golang.org/x/crypto/scrypt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"math/big"
	"reflect"
	"time"
)

type Keys struct {
	Public PublicKey
	Secret SecretKey
}
type PublicKey struct {
	N *big.Int
	E *big.Int
}
type SecretKey struct {
	N *big.Int
	D *big.Int
}

func GenerateKeyPair(keyLength int) *Keys {
	_, _, n, e, d := KeyGen(keyLength)
	return &Keys{
		PublicKey{N: n, E: e},
		SecretKey{N: n, D: d},
	}
}

func EncodePublicKey(key PublicKey) string {
	serialised, err := json.Marshal(&key)
	if err != nil {
		log.Panicf("Error encoding public key :/")
	}
	return string(serialised)
}

func DecodePublicKey(encodedKey string) *PublicKey {
	pk := PublicKey{}
	err := json.Unmarshal([]byte(encodedKey), &pk)
	if err != nil {
		log.Panicf("Error decoding public key :/")
	}
	return &pk
}

func encodeSecretKey(key SecretKey) string {
	serialised, err := json.Marshal(&key)
	if err != nil {
		log.Panicf("Error encoding secret key :/")
	}
	return string(serialised)
}

func decodeSecretKey(encodedKey string) *SecretKey {
	sk := SecretKey{}
	err := json.Unmarshal([]byte(encodedKey), &sk)
	if err != nil {
		log.Panicf("Error decoding secret key :/")
	}
	return &sk
}

func KeyGen(k int) (*big.Int, *big.Int, *big.Int, *big.Int, *big.Int) {
	if k < 16 {
		k = 16
	}

	var plen int = k >> 1
	var qlen int = k >> 1

	if k%2 != 0 {
		qlen = qlen + 1
	}

	e := big.NewInt(3)

	p, q, d := FindThoseNumbers(plen, qlen, e)

	for {
		if d.BitLen() != k { //keep looping till bitlength of d == k
			//fmt.Printf("bitLength of d != k, redoing. ")
			p, q, d = FindThoseNumbers(plen, qlen, e)

		} else {
			break
		}
	}
	n := big.NewInt(0).Mul(p, q)

	return p, q, n, e, d
}

func FindThoseNumbers(plen int, qlen int, e *big.Int) (*big.Int, *big.Int, *big.Int) {
	//TODO: Maybe check here that we don't halt and catch fire?
	gcdMismatches := 0
	PQCollisions := 0
	iterations := 0
	for { //keep going till we get two different primes.
		iterations++
		p, _ := rand.Prime(rand.Reader, plen)
		p_minus_1 := big.NewInt(0).Sub(p, big.NewInt(1))
		q, _ := rand.Prime(rand.Reader, qlen)
		q_minus_1 := big.NewInt(0).Sub(q, big.NewInt(1))

		mul := big.NewInt(0).Mul(p_minus_1, q_minus_1)

		if p.Cmp(q) != 0 { //check if the probable primes are different
			//then check if their GCDs with e are both 1:
			if checkGCDs(p_minus_1, q_minus_1, e) {
				d := big.NewInt(0).ModInverse(e, mul)
				//fmt.Printf(" Found P,Q,D after, %d iterations (%d GCD mismatches and %d P==Q occurences), ", iterations, gcdMismatches, PQCollisions)
				return p, q, d
			} else {
				//fmt.Printf("GCD mismatch: redoing,  ")
				gcdMismatches++
				continue
			}
		} else {
			//fmt.Println("P==Q: redoing, ")
			PQCollisions++
			continue
		}
	}
}

func checkGCDs(x *big.Int, y *big.Int, e *big.Int) bool {
	want := big.NewInt(1) //Want them all to be 1.

	GCD_XE := big.NewInt(0).GCD(nil, nil, x, e)

	GCD_YE := big.NewInt(0).GCD(nil, nil, y, e)

	if GCD_XE.Cmp(want) == 0 && GCD_YE.Cmp(want) == 0 {
		return true
	}
	return false
}

// Alice wants to send a message to Bob, so she encrypts it using Bob's public key
func Encrypt(m *big.Int, pk PublicKey) *big.Int {
	//c = m^e mod n
	c := big.NewInt(0).Exp(m, pk.E, pk.N) //e and n are part of the public key
	//fmt.Println("m is encrypted as c: ", c)
	return c
}

// Bob has received a message that was encrypted with his public key, so he decrypts it using his private key
func Decrypt(c *big.Int, sk SecretKey) *big.Int {
	//m = c^d mod n
	m := big.NewInt(0).Exp(c, sk.D, sk.N)
	//fmt.Println("m is decrypted as: ", m)
	return m
}

func EncryptToFile(filename string, plaintext string, key []byte) {

	plainByte := []byte(plaintext)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	//Makes ciphertext og pad vector as long as ciphertext
	ciphertext := make([]byte, aes.BlockSize+len(plainByte))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	//Encrypts ciphertext
	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plainByte)

	// Makes a new file
	err = ioutil.WriteFile(filename, ciphertext, 0700) //SET FILE PERMISSIONS TO USER ONLY
	if err != nil {
		fmt.Println(err)
	}
}

func DecryptFromFile(filename string, key []byte) string {

	ciphertext, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	iv := ciphertext[:aes.BlockSize]

	originalText := make([]byte, len(ciphertext))
	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(originalText, ciphertext[aes.BlockSize:])
	originalText = bytes.Trim(originalText, "\x00") //Trim off trailing null bytes.
	return string(originalText)
}

func HashBytes(m []byte) []byte {
	//Hash the message
	hash := sha256.New()
	_, err := hash.Write(m)
	if err != nil {
		panic("Hashing failed!")
	}
	//return byte array
	return hash.Sum(nil)
}

// Alice encrypts a message with Bobs public key, and signs its hash with her private key
func Signature(m *big.Int, sk SecretKey) *big.Int {
	//start := time.Now()

	hashBytes := HashBytes(m.Bytes())
	//Byte array to *big.Int
	hbi := big.NewInt(0).SetBytes(hashBytes)

	//Calculates s = h(m)^d mod n
	//s := Decrypt(hbi, sk) //d is the private key
	//NOTICE: signing == encryption is a property of vanilla RSA, and not a general rule.
	s := big.NewInt(0).Exp(hbi, sk.D, sk.N)
	//duration := time.Since(start)
	//fmt.Println("Time taken to sign: ", duration)
	return s
}

// bob verifies Alice's hash with her public key, (and can then decrypt her message with his private key)
func Verification(signature *big.Int, pk PublicKey, m *big.Int) bool {
	//Finds H(m) = s^e mod n

	//NOTICE: signing == encryption is a property of vanilla RSA, and not a general rule.
	//hm := Encrypt(signature, pk) //n and e are part of the public key

	hm := big.NewInt(0).Exp(signature, pk.E, pk.N) //e and n are part of the public key

	//Byte array to *big.Int
	hbi := big.NewInt(0).SetBytes(HashBytes(m.Bytes()))
	//Checks if calculated hbi equals hm from signature
	return reflect.DeepEqual(hbi, hm)
}

// Exercise 9.11 functions below:
func Generate(filename string, password string) string {
	entropy := passwordvalidator.GetEntropy(password) //https://github.com/wagslane/go-password-validator
	const minEntropyBits = 60
	err := passwordvalidator.Validate(password, minEntropyBits)
	if err != nil {
		panic(err)
	}
	fmt.Printf("That's a nice password with a calculated %f bits of entropy..\nChecking if it's in known leaks using the enzoic api...\n", entropy)

	//Please don't steal my API keys...
	enzoicClient, err := enzoic.NewClient("5d1d0c94b5214e0c8826b699bcd6a94d", "&TJmBM5U7^KJ8c_5#6PJ!4J6R@*FjKy!")
	if err != nil {
		panic(err)
	}
	passwordCompromised, err := enzoicClient.CheckPassword(password)

	if passwordCompromised {
		panic("Password is compromised! Don't use it!\n")
	} else {
		fmt.Println("Password seems safe. proceeding..")
	}
	newKeys := GenerateKeyPair(2048)

	//Using a microsecond timestamp as salt, because this can be stored as file metadata
	//WAITAMIINUTE: THIS BREAKS AVAILABILITY if file is moved, copied, modified. Dropping the idea
	//currentTime := time.Now()
	//microSecondBytes := []byte(strconv.FormatInt(currentTime.UnixMicro(), 16)) //I.V.: not truly random, but recoverable from the file metadata
	//salt := HashBytes(microSecondBytes)                                        //hash the timestamp bytes for good measure
	//Using a concatenation of filename and milliseconds as input for salt hash instead:
	start := time.Now()
	salt := HashBytes([]byte(filename))
	fmt.Printf("Salt: %+v\n", salt)
	dk, err := scrypt.Key([]byte(password), salt, 32768, 8, 1, 32) // https://pkg.go.dev/golang.org/x/crypto/scrypt
	duration := time.Since(start)
	if err != nil {
		panic(err)
	}
	fmt.Printf("It took scrypt %s to derive a 32byte AES key from the password...\n", duration)
	fmt.Printf("Which means that you can try and bruteforce the password at a whooping %f attempts pr second... Good luck ;)\n", 1/duration.Seconds())
	fmt.Printf("Estimated time to crack (half of gauranteed time to crack): %e years\n", (math.Pow(2, entropy)/(1.0/duration.Seconds()))/2.0/31536000)
	fmt.Printf("You'd probably have better luck using a wordlist...\n")
	EncryptToFile(filename, encodeSecretKey(newKeys.Secret), dk)
	//_ = os.Chtimes(filename, *new(time.Time), currentTime) //update file modify timestamp
	return EncodePublicKey(newKeys.Public)

}

func Sign(filename string, password string, message []byte) big.Int {
	fmt.Printf("Attempting to sign message with private key from encrypted file: %s\n", filename)
	salt := HashBytes([]byte(filename)) //sha256 hash of the filename used as "unique" I.V.
	fmt.Printf("Salt: %+v\n", salt)
	start := time.Now()
	dk, err := scrypt.Key([]byte(password), salt, 32768, 8, 1, 32) // https://pkg.go.dev/golang.org/x/crypto/scrypt
	duration := time.Since(start)
	if err != nil {
		panic(err)
	}
	fmt.Printf("It took scrypt %s to derive a 32byte AES key from the password.\n", duration)
	start = time.Now()
	secretKey := decodeSecretKey(DecryptFromFile(filename, dk)) //read file, decrypt, unmarshal
	loadKeyDuration := time.Since(start)
	fmt.Printf("It took %s to decrypt and parse the private key.\n", loadKeyDuration)
	start = time.Now()
	mh := big.NewInt(0).SetBytes(HashBytes(message)) //finding message hash h(m):
	smh := Signature(mh, *secretKey)                 //Signing h(m) with sk
	signDuration := time.Since(start)
	fmt.Printf("It took %s to sign the message.\n", signDuration)
	return *smh //return the signature s(h(m))
}
