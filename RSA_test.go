package RSA

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"os"
	"reflect"
	"testing"
	"time"
)

var kMap map[int]int
var pMap map[int]*big.Int
var qMap map[int]*big.Int
var nMap map[int]*big.Int
var dMap map[int]*big.Int

func TestGenerateVar(t *testing.T) {
	fmt.Print("Testing RSA key generation from 100 bits to 5000 bits (with 501 bit increments).. this will take a minute... ")
	kMap = make(map[int]int)
	pMap = make(map[int]*big.Int)
	qMap = make(map[int]*big.Int)
	nMap = make(map[int]*big.Int)
	dMap = make(map[int]*big.Int)
	i := 1
	for k := 100; k < 5000; k = k + 501 {
		fmt.Printf("K:%d, ", k)
		p, q, n, _, d := KeyGen(k)
		kMap[i] = k
		pMap[i] = p
		qMap[i] = q
		nMap[i] = n
		dMap[i] = d
		i++
	}
}

func TestNLength(t *testing.T) {
	fmt.Print("Testing lenght of n... ")

	for i, n := range nMap {
		// t.Logf("N test round %d", i)
		os.Stdout.Sync()
		got := n.BitLen()
		want := kMap[i]

		if got != want {
			t.Errorf("Bit length of n should be %d, but was %d\n", want, got)
		} else {
			//fmt.Println("OK!")
		}
	}

}

func TestDLength(t *testing.T) {
	fmt.Print("Testing lenght of d... ")

	os.Stdout.Sync()
	for i, d := range dMap {
		k := kMap[i]
		// p := pMap[i]
		// q := qMap[i]

		// t.Logf("\nLoop:%d\n k = %d \n p len = %d\n p = %d\n q len = %d\n q = %d\n d len = %d\n", i, k, p.BitLen(), p, q.BitLen(), q, d.BitLen())
		os.Stdout.Sync()

		got := d.BitLen()
		want := k

		if got != want {
			t.Errorf("!!!!!Bit length of d should be %d, but was %d!!!!!\n", want, got)
		} else {
			//fmt.Println("OK!")
		}
	}
}

func TestEncryptDecryptRSA(t *testing.T) {
	fmt.Print("Testing RSA encryption and decryption... ")
	//_, _, n, e, d := KeyGen(16)
	myKeyPair := GenerateKeyPair(2048)
	m := big.NewInt(250)
	c := Encrypt(m, myKeyPair.Public)
	mDec := Decrypt(c, myKeyPair.Secret)
	if mDec.Cmp(m) != 0 {
		t.Errorf("Decrypted message should be %d, but was %d", m, mDec)
	} else {
		fmt.Println("OK!")
	}
}

func TestEncryptDecryptToFile(t *testing.T) {
	fmt.Print("Testing encrypt and decrypt to/from file... ")
	//Making key of correct length
	key := []byte("jcqclnpuomuimugwzyeghvsydaqppdod") //32 bytes
	// Strings for encryption
	plaintext := "Encryption and decryption is fun and it works!"
	filename := "testFilePleaseIgnore"

	//Encryption:
	EncryptToFile(filename, plaintext, key)

	//Decryption
	message_after_decryption := DecryptFromFile(filename, key)

	//Comparison
	comparison := message_after_decryption == plaintext
	//print(comparison)
	if comparison == false {
		t.Errorf("Wrong encryption. \n Before: %s \n After: %s", plaintext, message_after_decryption)
	} else {
		fmt.Println("OK!")
	}

	//TODO: cleanup of files?

}

func TestEncryptDecryptOfRSAKeyToFile(t *testing.T) {
	fmt.Print("Testing storing and loading encrypted RSA key to/from file... ")

	//_, _, n, e, d := KeyGen(2048)
	myKeyPair := GenerateKeyPair(2000)

	m := big.NewInt(1337)
	//Encrypt a message with the generated keys
	c := Encrypt(m, myKeyPair.Public)

	//Making AES_key for file-generation
	AES_key := []byte("jcqclnpuomuimugwzyeghvsydaqppdod") //32 bytes

	plaintext_key := encodeSecretKey(myKeyPair.Secret)

	filename := "RSAfile"
	//Encrypting secret key to file
	EncryptToFile(filename, plaintext_key, AES_key)
	//Decrypting secret key from file
	skPlaintextRaw := DecryptFromFile(filename, AES_key)

	recoveredSecretKey := decodeSecretKey(skPlaintextRaw)

	//Using AES decrypted recovered key to decrypt the original message
	mDec := Decrypt(c, *recoveredSecretKey)

	if mDec.Cmp(m) != 0 {
		t.Errorf("Decrypted message should be %d, but was %d", m, mDec)
	} else {
		fmt.Println("OK!")
	}
}

func TestSignature(t *testing.T) {
	fmt.Println("Testing verification and correct rejection of signed / modified messages:")
	message := big.NewInt(1337)
	//_, _, n, e, d := KeyGen(2000)
	myKeyPair := GenerateKeyPair(2000)

	//	signature_m := Signature(message, e, n)
	signature_m := Signature(message, myKeyPair.Secret)
	fmt.Print("Testing verification of signed message: ")
	//if !Verification(signature_m, n, e, d, message) {
	if !Verification(signature_m, myKeyPair.Public, message) {
		t.Errorf("Verification of message: %d with signature %d failed!", message, signature_m)
	} else {
		fmt.Printf("OK!\n")
	}

	modified_message := big.NewInt(1338)
	fmt.Print("Testing correct signature rejection of tampered message: ")
	//if Verification(signature_m, n, e, d, modified_message) {
	if Verification(signature_m, myKeyPair.Public, modified_message) {
		t.Errorf("Bad message verified with good signature!")
	} else {
		fmt.Printf("OK!\n")
	}

}

func Test_FindAverageHashingTime(t *testing.T) {
	var size int = 10000
	var iterations int = 8192
	fmt.Printf("Hashing %d bytes %d times to get average speed...\n", size, iterations)
	//generate sets of random data:
	m := make([][]byte, size)
	for i := 0; i < iterations; i++ {
		m[i] = make([]byte, size)
		rand.Read(m[i])
	}

	var sum float64 = 0.0
	for i := 0; i < iterations; i++ {
		//rand.Read(m[i])
		start := time.Now()
		HashBytes(m[i])
		duration := time.Since(start)
		speed := float64(size) * 8.0 / duration.Seconds() //Check if gives precision!!!!
		sum += speed
	}
	var avg float64 = sum / float64(iterations)
	fmt.Printf("Average speed of %d hashings of %d bytes: %f bits/second\n", iterations, size, avg)
}

func TestCalculateHashedRSASignatureSpeed(t *testing.T) {
	var iterations int = 2048

	//Prep the data:
	m := make([]*big.Int, iterations)
	for i := 0; i < iterations; i++ {
		newInt, _ := rand.Int(rand.Reader, big.NewInt(1337))
		m[i] = newInt
	}
	//_, _, n, _, d := KeyGen(2000)
	myKeyPair := GenerateKeyPair(2000)

	var sum float64 = 0.0
	for i := 0; i < iterations; i++ {
		start := time.Now()
		Signature(m[i], myKeyPair.Secret)
		duration := time.Since(start)
		speed := float64(m[i].BitLen()) / duration.Seconds()
		sum += speed
	}
	var avg float64 = sum / float64(iterations)
	fmt.Printf("Average signinig speed after signing %d hashed messages: %f bits/second\n", iterations, avg)

}

func TestCalculateRSASignatureSpeed(t *testing.T) {
	var integers int = 2048

	//Prep the data:
	m := make([]big.Int, integers)
	for i := 0; i < integers; i++ {
		newInt, _ := rand.Int(rand.Reader, big.NewInt(1337))
		m[i] = *newInt
	}
	//	_, _, n, _, d := KeyGen(2000)
	myKeyPair := GenerateKeyPair(2000)

	var sum float64 = 0.0
	for i := 0; i < integers; i++ {
		start := time.Now()
		_ = Decrypt(&m[i], myKeyPair.Secret) //pretend that signature is s(m) instead of s(h(m))
		duration := time.Since(start)
		speed := float64(m[i].BitLen()) / duration.Seconds()
		sum += speed
	}
	var avg float64 = sum / float64(integers)
	fmt.Printf("Average signinig speed after signing %d 64 bit integers: %f bits/second\n", integers, avg)

}

func TestEncodeDecodePubKey(t *testing.T) {
	keys := GenerateKeyPair(2048)

	publicStr := EncodePublicKey(keys.Public)
	//fmt.Printf("Encoded public key: %s\n", publicStr)
	decoded := *DecodePublicKey(publicStr)

	if !reflect.DeepEqual(keys.Public, decoded) {
		t.Errorf("Decoded public key does not match initial input:\nOriginal: %+v\nDecoded: %+v", keys.Public, decoded)
	}
}

func TestGenerate(t *testing.T) {
	pk := Generate("TestGenerate.txt", "FURTHERMORERANDOMSTUFFISTYPEDINTOCOMPUTERS")
	fmt.Printf("Success! Public key is: %+v\n", pk)
}

func TestRecoverSecretKey(t *testing.T) {
	_ = Sign("TestGenerate.txt", "FURTHERMORERANDOMSTUFFISTYPEDINTOCOMPUTERS", []byte("Just some message"))
}

func TestWrongPass(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic, when it should have failed to retrieve the RSA secret key. (bad password)")
		}
	}()
	filename := "hello.txt"
	_ = Generate(filename, "FURTHERMORERANDOMSTUFFISTYPEDINTOCOMPUTERS")

	_ = Sign(filename, "wrong password", []byte("Just some message"))
}

func TestWeakPassRejection(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic, when it should have rejected a low-entropy password!")
		}
	}()
	filename := "hello.txt"
	_ = Generate(filename, "password") //low entropy

}
func TestLeakedPassRejection(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic, when it should have rejected a compromised password!")
		}
	}()
	filename := "hello.txt"
	_ = Generate(filename, "Correct Horse Battery Staple")

}

func TestSign(t *testing.T) {
	pass := "H!IJMY,ATIC,BHMN,SCMM?" //"Hey! I Just Met You, And This Is Crazy, But Here's My Number, So Call Me Maybe?"
	//pass := "password" //low-entropy password
	//pass := "passwordpassword" //compromised password
	filename := "salted_scrypted_password_to_AES_encrypted_privateKey_for_test.bin"

	fmt.Printf("Generating keys, and encrypting secret key to file...\n")
	pk := DecodePublicKey(Generate(filename, pass))

	fmt.Printf("\nSigning...\n\n")
	m := []byte("Test Message to be signed")
	mh := big.NewInt(0).SetBytes(HashBytes(m))

	s_m := Sign(filename, pass, m)
	fmt.Printf("Signature s(h(m)) (type: %T) bytes (in base 16): %x\n", s_m, s_m.Int64())
	if !Verification(&s_m, *pk, mh) {
		t.Error("Failed to verify signed message!\n")
	} else {
		fmt.Printf("Done!\n")
	}
}
