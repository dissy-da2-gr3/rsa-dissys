module RSA

go 1.21

require (
	github.com/enzoic/enzoic-go-client v1.2.0
	github.com/wagslane/go-password-validator v0.3.0
	golang.org/x/crypto v0.13.0
)

require (
	github.com/jzelinskie/whirlpool v0.0.0-20201016144138-0675e54bb004 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
